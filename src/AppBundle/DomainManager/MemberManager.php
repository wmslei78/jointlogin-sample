<?php

namespace AppBundle\DomainManager;

use AppBundle\Entity\Member;
use AppBundle\Repository\MemberRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Wmslei78\Bundle\JointLoginBundle\Component\UserManagerInterface;

class MemberManager implements UserManagerInterface
{
    private $em;

    private $repos;


    public function __construct( EntityManager $em,
                                 MemberRepository $repos )
    {
        $this->em = $em;
        $this->repos = $repos;
    }

    public function save( $platform, $uid, $nickname, $portrait )
    {
        $username = $platform.'/'.$uid;
        $member = $this->findByUsername( $username );
        if ( !$member ) {
            $member = new Member();
            $member->setCreatedAt( new \DateTime() );
        }

        $member
            ->setUsername( $username )
            ->setNickname( $nickname )
            ->setPortrait( $portrait )
            ;

        $this->em->persist( $member );
        $this->em->flush();

        return $username;
    }

    public function findByUsername( $username )
    {
        return $this->repos->findOneByUsername( $username );
    }
}
