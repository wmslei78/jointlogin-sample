<?php

namespace AppBundle\DomainManager;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SecurityManager
{
    private $tokenStorage;

    public function __construct( TokenStorageInterface $tokenStorage )
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function getUser()
    {
        $token = $this->tokenStorage->getToken();

        if ( null !== $token &&
             is_object($token->getUser()) )
            return $token->getUser();
    }
}
