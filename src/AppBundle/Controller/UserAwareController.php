<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Member;

trait UserAwareController
{
    protected $user;


    public function setUser( Member $user = null )
    {
        $this->user = $user;
    }
}
