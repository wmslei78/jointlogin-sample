<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\RouteResource;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @RouteResource("profile")
 */
class ProfileController
{

    use UserAwareController;

    public function getAction()
    {
        return $this->user;
    }

}
